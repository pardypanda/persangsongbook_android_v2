package com.pardypanda.songbook;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import java.io.IOException;

import Database.DatabaseHelper;
import Utils.Constants;


public class HomeActivity extends ActionBarActivity {

    DatabaseHelper dbHelper;

    RelativeLayout productSkipper;
    RelativeLayout productNDP;
    RelativeLayout productDZP;
    RelativeLayout productDZ;
    RelativeLayout productNDZT;
    RelativeLayout productASP;
    RelativeLayout productAS;
    RelativeLayout productNRVPlus;
    RelativeLayout productNRV;
    RelativeLayout productRV;

    RelativeLayout productVol2;
    RelativeLayout productVol3;
    RelativeLayout productVol4;
    RelativeLayout productVol5;
    RelativeLayout productVol6;
    RelativeLayout productVol7;
    RelativeLayout productVol8;
    RelativeLayout productVol9;
    RelativeLayout productVol10;
    RelativeLayout productVol11;
    RelativeLayout productVol3456;
    RelativeLayout productVol34567;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initDB();
        setupUI();
    }

    private void initDB() {
        dbHelper = new DatabaseHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void setupUI() {

        productSkipper = (RelativeLayout) findViewById(R.id.productSkipper);
        productNDP = (RelativeLayout) findViewById(R.id.productNDZP);
        productDZP = (RelativeLayout) findViewById(R.id.productDZP);
        productDZ = (RelativeLayout) findViewById(R.id.productDZ);
        productNDZT = (RelativeLayout) findViewById(R.id.productNDZT);
        productASP = (RelativeLayout) findViewById(R.id.productASP);
        productAS = (RelativeLayout) findViewById(R.id.productAS);
        productNRVPlus = (RelativeLayout) findViewById(R.id.productNRVPlus);
        productNRV = (RelativeLayout) findViewById(R.id.productNRV);
        productRV = (RelativeLayout) findViewById(R.id.productRV);

//        productVol2 = (RelativeLayout) findViewById(R.id.productVol2);
        productVol3 = (RelativeLayout) findViewById(R.id.productVol3);
        productVol4 = (RelativeLayout) findViewById(R.id.productVol4);
        productVol5 = (RelativeLayout) findViewById(R.id.productVol5);
        productVol6 = (RelativeLayout) findViewById(R.id.productVol6);
        productVol7 = (RelativeLayout) findViewById(R.id.productVol7);
        productVol8 = (RelativeLayout) findViewById(R.id.productVol8);
        productVol9 = (RelativeLayout) findViewById(R.id.productVol9);
        productVol10 = (RelativeLayout) findViewById(R.id.productVol10);
        productVol11 = (RelativeLayout) findViewById(R.id.productVol11);
//        productVol3456 = (RelativeLayout) findViewById(R.id.productVol3456);
//        productVol34567 = (RelativeLayout) findViewById(R.id.productVol34567);

        productSkipper.setOnClickListener(clickListener);
        productNDP.setOnClickListener(clickListener);
        productDZP.setOnClickListener(clickListener);
        productDZ.setOnClickListener(clickListener);
        productNDZT.setOnClickListener(clickListener);
        productASP.setOnClickListener(clickListener);
        productAS.setOnClickListener(clickListener);
        productNRVPlus.setOnClickListener(clickListener);
        productNRV.setOnClickListener(clickListener);
        productRV.setOnClickListener(clickListener);

//        productVol2.setOnClickListener(clickListener);
        productVol3.setOnClickListener(clickListener);
        productVol4.setOnClickListener(clickListener);
        productVol5.setOnClickListener(clickListener);
        productVol6.setOnClickListener(clickListener);
        productVol7.setOnClickListener(clickListener);
        productVol8.setOnClickListener(clickListener);
        productVol9.setOnClickListener(clickListener);
        productVol10.setOnClickListener(clickListener);
        productVol11.setOnClickListener(clickListener);
//        productVol3456.setOnClickListener(clickListener);
//        productVol34567.setOnClickListener(clickListener);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Intent intentProductsSongsList = new Intent();
            intentProductsSongsList.setClass(HomeActivity.this, SongsListActivity.class);

            switch (view.getId()) {
                case R.id.productSkipper:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_SKIPPER);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productNDZP:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_NEW_DZIRE_PLUS);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productDZP:
                    showProductSerialSelectionDialog(Constants.PRODUCT_CODE_DZIRE_PLUS, Constants.PRODUCT_CODE_DZIRE_PLUS_T);
                    break;

                case R.id.productDZ:
                    showProductSerialSelectionDialog(Constants.PRODUCT_CODE_DZIRE, Constants.PRODUCT_CODE_DZIRE_T);
                    break;

                case R.id.productNDZT:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_NEW_DZIRE_T);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productASP:
                    showProductSerialSelectionDialog(Constants.PRODUCT_CODE_ASPIRE_PLUS, Constants.PRODUCT_CODE_ASPIRE_PLUS_T);
                    break;

                case R.id.productAS:
                    showProductSerialSelectionDialog(Constants.PRODUCT_CODE_ASPIRE, Constants.PRODUCT_CODE_ASPIRE_T);
                    break;

                case R.id.productNRVPlus:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_NEW_REVOLUTION_PLUS);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productNRV:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_NEW_REVOLUTION);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productRV:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_REVOLUTION);
                    startActivity(intentProductsSongsList);
                    break;

//                case R.id.productVol2:
//                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL2);
//                    startActivity(intentProductsSongsList);
//                    break;

                case R.id.productVol3:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL3);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol4:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL4);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol5:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL5);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol6:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL6);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol7:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL7);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol8:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL8);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol9:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL9);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol10:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL10);
                    startActivity(intentProductsSongsList);
                    break;

                case R.id.productVol11:
                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL11);
                    startActivity(intentProductsSongsList);
                    break;

//                case R.id.productVol3456:
//                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL3456);
//                    startActivity(intentProductsSongsList);
//                    break;
//
//                case R.id.productVol34567:
//                    intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_CODE_VOL34567);
//                    startActivity(intentProductsSongsList);
//                    break;
            }

        }
    };

    private void showProductSerialSelectionDialog(String option1, String option2) {

        final CharSequence[] products = new CharSequence[]{option1, option2};
        final CharSequence[] productsView = new CharSequence[]{option1 + " XXXX", option2 + " XXXX"};

        final AlertDialog dialogSelectProduct;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Select Serial Number");
        dialogBuilder.setSingleChoiceItems(
                productsView, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        Intent intentProductsSongsList = new Intent();
                        intentProductsSongsList.setClass(HomeActivity.this, SongsListActivity.class);
                        intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, products[item].toString());
                        startActivity(intentProductsSongsList);

                        dialog.dismiss();
                    }
                });

        dialogSelectProduct = dialogBuilder.create();
        dialogSelectProduct.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorites) {
            Intent intentProductsSongsList = new Intent();
            intentProductsSongsList.setClass(HomeActivity.this, SongsListActivity.class);
            intentProductsSongsList.putExtra(Constants.KEY_PRODUCT_CODE, Constants.PRODUCT_FAVORITES);
            startActivity(intentProductsSongsList);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
