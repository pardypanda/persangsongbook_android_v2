package com.pardypanda.songbook;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.logging.Handler;

import Database.DatabaseHelper;
import Utils.Constants;
import Utils.SongsCursorAdapter;


public class SongsListActivity extends ActionBarActivity {

    String selectedProduct;

    ListView mListSongs;
    TextView txtEmpty;

    DatabaseHelper mDBHelper;
    SongsCursorAdapter mSongsCursorAdapter;
    Cursor mCursor;

    boolean mSearchOpened;
    String mSearchQuery;
    Drawable mIconOpenSearch;
    Drawable mIconCloseSearch;
    EditText mSearchEt;
    MenuItem mSearchAction;
    InputMethodManager mInputMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs_list);

        mInputMgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (getIntent().getStringExtra(Constants.KEY_PRODUCT_CODE) != null
                && !getIntent().getStringExtra(Constants.KEY_PRODUCT_CODE).isEmpty()) {
            selectedProduct = getIntent().getStringExtra(Constants.KEY_PRODUCT_CODE);
        } else {
            selectedProduct = Constants.PRODUCT_CODE_NEW_DZIRE_PLUS;
        }

        getSupportActionBar().setTitle(Constants.mapProducts.get(selectedProduct));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupUI();

        populateList();
    }

    private void setupUI() {
        mDBHelper = new DatabaseHelper(this);
        try {
            mDBHelper.openDataBase();
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        mIconOpenSearch = getResources()
                .getDrawable(R.drawable.abc_ic_search_api_mtrl_alpha);
        mIconCloseSearch = getResources()
                .getDrawable(R.drawable.abc_ic_clear_mtrl_alpha);

        mListSongs = (ListView) findViewById(R.id.listView);
        txtEmpty = (TextView) findViewById(R.id.txtEmpty);
    }

    private void populateList() {

        final ProgressDialog progress = new ProgressDialog(SongsListActivity.this);

        AsyncTask<Void, Void, Void> taskPopulateList = new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setMessage("Loading ...");
                progress.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                if (selectedProduct.equals(Constants.PRODUCT_FAVORITES)) {
                    mCursor = mDBHelper.getFavorites();
                } else {
                    mCursor = mDBHelper.getSongs(selectedProduct);
                }
                mSongsCursorAdapter = new SongsCursorAdapter(SongsListActivity.this, mCursor, 0, selectedProduct);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (mCursor.getCount() > 0) {
                    mListSongs.setAdapter(mSongsCursorAdapter);
                    mListSongs.setVisibility(View.VISIBLE);
                    txtEmpty.setVisibility(View.GONE);

                    mSongsCursorAdapter.setFilterQueryProvider(new FilterQueryProvider() {
                        @Override
                        public Cursor runQuery(CharSequence charSequence) {
                            return mDBHelper.filterQuery(selectedProduct, charSequence);
                        }
                    });
                } else {
                    mListSongs.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.VISIBLE);
                }

                progress.dismiss();
            }
        }.execute();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_songs_list, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        if (id == R.id.action_search) {
            if (mSearchOpened) {
                closeSearchBar();
            } else {
                openSearchBar(mSearchQuery);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openSearchBar(String queryText) {

        // Set custom view on action bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.search_bar);

        // Search edit text field setup.
        mSearchEt = (EditText) actionBar.getCustomView()
                .findViewById(R.id.etSearch);
        mSearchEt.addTextChangedListener(new SearchWatcher());
        mSearchEt.setText(queryText);
        mSearchEt.requestFocus();
        mInputMgr.showSoftInput(mSearchEt, InputMethodManager.SHOW_IMPLICIT);
        // mInputMgr.hideSoftInputFromWindow(mSearchEt.getWindowToken(), 0);

        // Change search icon accordingly.
        mSearchAction.setIcon(mIconCloseSearch);
        mSearchOpened = true;

    }

    private void closeSearchBar() {

        // Remove custom view.
        getSupportActionBar().setDisplayShowCustomEnabled(false);

        // Change search icon accordingly.
        mSearchAction.setIcon(mIconOpenSearch);
        mSearchOpened = false;

        mSearchEt.setText("");

    }

    @Override
    protected void onDestroy() {
        //mDBHelper.close();
        super.onDestroy();
    }

    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            mSongsCursorAdapter.filter(mSearchEt.getText().toString());
            mSongsCursorAdapter.notifyDataSetChanged();
        }

    }
}
