package Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.pardypanda.songbook.R;

import java.text.Normalizer;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tanay on 24/03/15.
 */
public class SongsCursorAdapter extends CursorAdapter implements SectionIndexer {

    AlphabetIndexer mAlphabetIndexer;
    LayoutInflater mInflater;
    Set<String> setFavorites;
    String selectedProduct;

    SharedPreferences mSharedPrefs;
    SharedPreferences.Editor mEditor;

    String searchString = "";

    public SongsCursorAdapter(Context context, Cursor c, int flags, String selectedProduct) {
        super(context, c, flags);

        mSharedPrefs = context.getSharedPreferences(Constants.KEY_SHARED_PREFS, Context.MODE_PRIVATE);
        mEditor = mSharedPrefs.edit();
        setFavorites = mSharedPrefs.getStringSet(Constants.KEY_SET_FAVORITES, null);

        mInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        mAlphabetIndexer = new AlphabetIndexer(c,
                c.getColumnIndex(Constants.COLUMN_SONG_NAME),
                "ABCDEFGHIJKLMNOPQRTSUVWXYZ0123456789");
        mAlphabetIndexer.setCursor(c);

        this.selectedProduct = selectedProduct;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

        View initialisedView = mInflater.inflate(R.layout.list_item_song_detail, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder();
        viewHolder.txtSongName = (TextView) initialisedView.findViewById(R.id.txtSongName);
        viewHolder.txtSongNumber = (TextView) initialisedView.findViewById(R.id.txtSongNumber);
        viewHolder.txtSongLanguage = (TextView) initialisedView.findViewById(R.id.txtLanguage);
        viewHolder.txtSongSinger = (TextView) initialisedView.findViewById(R.id.txtSongSinger);
        viewHolder.txtSongComposer = (TextView) initialisedView.findViewById(R.id.txtSongComposer);
        viewHolder.txtSongAlbum = (TextView) initialisedView.findViewById(R.id.txtSongAlbum);
        viewHolder.txtProductName = (TextView) initialisedView.findViewById(R.id.txtProduct);
        viewHolder.icStar = (ImageView) initialisedView.findViewById(R.id.ic_star);

        viewHolder.indexSongName = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_NAME);
        viewHolder.indexSongNumber = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_NUMBER);
        viewHolder.indexSongLanguage = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_LANGUAGE);
        viewHolder.indexSongSinger = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_SINGER);
        viewHolder.indexSongComposer = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_COMPOSER);
        viewHolder.indexSongAlbum = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_ALBUM);
        viewHolder.indexProductName = cursor.getColumnIndexOrThrow(Constants.COLUMN_SONG_PRODUCT);

        initialisedView.setTag(viewHolder);

        return initialisedView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        viewHolder.txtSongName.setText(highlight(cursor.getString(viewHolder.indexSongName)));
        viewHolder.txtSongNumber.setText(highlight(cursor.getString(viewHolder.indexSongNumber)));
        viewHolder.txtSongLanguage.setText(highlight(cursor.getString(viewHolder.indexSongLanguage)));
        viewHolder.txtSongSinger.setText(highlight(cursor.getString(viewHolder.indexSongSinger)));
        viewHolder.txtSongComposer.setText(highlight(cursor.getString(viewHolder.indexSongComposer)));
        viewHolder.txtSongAlbum.setText(highlight(cursor.getString(viewHolder.indexSongAlbum)));
        viewHolder.txtProductName.setText(highlight(cursor.getString(viewHolder.indexProductName)));
        viewHolder.icStar.setId(Integer.parseInt(cursor.getString(viewHolder.indexSongNumber)));

        if (setFavorites != null && setFavorites.size() > 0) {
            if (setFavorites.contains(cursor.getString(viewHolder.indexSongNumber))) {
                viewHolder.icStar.setImageResource(R.drawable.star_filled);
            } else {
                viewHolder.icStar.setImageResource(R.drawable.star_empty);
            }
        }

        viewHolder.icStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgStar = (ImageView) view;
                if (setFavorites == null) {
                    setFavorites = new HashSet<String>();
                }

                if (setFavorites.contains(Integer.toString(view.getId()))) {
                    setFavorites.remove(Integer.toString(view.getId()));
                    imgStar.setImageResource(R.drawable.star_empty);
                } else {
                    setFavorites.add(Integer.toString(view.getId()));
                    imgStar.setImageResource(R.drawable.star_filled);
                }

                mEditor.clear();
                mEditor.putStringSet(Constants.KEY_SET_FAVORITES, setFavorites);
                mEditor.commit();
            }
        });

        if (selectedProduct.equals(Constants.PRODUCT_FAVORITES)) {
            viewHolder.txtProductName.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.txtProductName.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public Object[] getSections() {
        return mAlphabetIndexer.getSections();
    }

    @Override
    public int getPositionForSection(int i) {
        return mAlphabetIndexer.getPositionForSection(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return mAlphabetIndexer.getSectionForPosition(i);
    }

    @Override
    public void changeCursor(Cursor cursor) {
        mAlphabetIndexer.setCursor(cursor);
        super.changeCursor(cursor);
    }

    public void filter(CharSequence constraint) {
        getFilter().filter(constraint);
        this.searchString = constraint.toString();
    }

    // Function to highlight searched string
    public CharSequence highlight(String originalText) {
        String search = this.searchString;

        if (!search.isEmpty()) {
            // ignore case and accents
            // the same thing should have been done for the search text
            String normalizedText = Normalizer.normalize(originalText, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();

            int start = normalizedText.indexOf(search);
            if (start < 0) {
                // not found, nothing to to
                return originalText;
            } else {
                // highlight each appearance in the original text
                // while searching in normalized text
                Spannable highlighted = new SpannableString(originalText);
                while (start >= 0) {
                    int spanStart = Math.min(start, originalText.length());
                    int spanEnd = Math.min(start + search.length(), originalText.length());

                    ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.CYAN});
                    TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);

                    highlighted.setSpan(highlightSpan, spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    start = normalizedText.indexOf(search, spanEnd);
                }

                return highlighted;
            }
        } else {
            return originalText;
        }
    }

    class ViewHolder {
        ImageView icStar;
        TextView txtSongName;
        TextView txtSongNumber;
        TextView txtSongLanguage;
        TextView txtSongSinger;
        TextView txtSongComposer;
        TextView txtSongAlbum;
        TextView txtProductName;

        int indexSongName;
        int indexSongNumber;
        int indexSongLanguage;
        int indexSongSinger;
        int indexSongComposer;
        int indexSongAlbum;
        int indexProductName;
    }
}
