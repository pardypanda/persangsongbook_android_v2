package Utils;

import java.util.HashMap;

/**
 * Created by Tanay on 24/03/15.
 */
public class Constants {

    public final static String KEY_SHARED_PREFS = "SongBankPrefs";
    public final static String KEY_PRODUCT_CODE = "productCode";
    public final static String KEY_SET_FAVORITES = "keyFavorites";

    public final static  String PRODUCT_CODE_SKIPPER = "Skipper";
    public final static  String PRODUCT_CODE_NEW_DZIRE_PLUS = "NDZPT";
    public final static  String PRODUCT_CODE_DZIRE_PLUS = "DZP";
    public final static  String PRODUCT_CODE_DZIRE_PLUS_T = "DZPT";
    public final static  String PRODUCT_CODE_DZIRE = "DZ";
    public final static  String PRODUCT_CODE_DZIRE_T = "DZT";
    public final static  String PRODUCT_CODE_NEW_DZIRE_T = "NDZT";
    public final static  String PRODUCT_CODE_ASPIRE_PLUS = "ASP";
    public final static  String PRODUCT_CODE_ASPIRE_PLUS_T = "ASPT";
    public final static  String PRODUCT_CODE_ASPIRE = "AS";
    public final static  String PRODUCT_CODE_ASPIRE_T = "AST";
    public final static  String PRODUCT_CODE_NEW_REVOLUTION = "NRV";
    public final static  String PRODUCT_CODE_NEW_REVOLUTION_T = "NRVT";
    public final static  String PRODUCT_CODE_NEW_REVOLUTION_PLUS = "NRVPLUS";
    public final static  String PRODUCT_CODE_REVOLUTION = "RV";

    public final static  String PRODUCT_CODE_VOL2 = "Vol-2";
    public final static  String PRODUCT_CODE_VOL3 = "Vol-3";
    public final static  String PRODUCT_CODE_VOL4 = "Vol-4";
    public final static  String PRODUCT_CODE_VOL5 = "Vol-5";
    public final static  String PRODUCT_CODE_VOL6 = "Vol-6";
    public final static  String PRODUCT_CODE_VOL7 = "Vol-7";
    public final static  String PRODUCT_CODE_VOL8 = "Vol-8";
    public final static  String PRODUCT_CODE_VOL9 = "Vol-9";
    public final static  String PRODUCT_CODE_VOL10 = "Vol-10";
    public final static  String PRODUCT_CODE_VOL11 = "Vol-11"; // Collection of 3-4-5-6-7-8-9
    public final static  String PRODUCT_CODE_VOL3456 = "Volume-3-4-5-6";
    public final static  String PRODUCT_CODE_VOL34567 = "Volume-3-4-5-6-7";

    public final static String PRODUCT_FAVORITES = "Favorites";

    public final static HashMap<String, String> mapProducts;
    static {
        mapProducts = new HashMap<String, String>();
        mapProducts.put(PRODUCT_CODE_SKIPPER, "Skipper");
        mapProducts.put(PRODUCT_CODE_NEW_DZIRE_PLUS, "New Dzire Plus");
        mapProducts.put(PRODUCT_CODE_DZIRE_PLUS, "Dzire Plus");
        mapProducts.put(PRODUCT_CODE_DZIRE_PLUS_T, "Dzire Plus");
        mapProducts.put(PRODUCT_CODE_DZIRE, "Dzire");
        mapProducts.put(PRODUCT_CODE_DZIRE_T, "Dzire");
        mapProducts.put(PRODUCT_CODE_DZIRE_T, "New Dzire");
        mapProducts.put(PRODUCT_CODE_ASPIRE_PLUS, "Aspire Plus");
        mapProducts.put(PRODUCT_CODE_ASPIRE_PLUS_T, "Aspire Plus");
        mapProducts.put(PRODUCT_CODE_ASPIRE, "Aspire");
        mapProducts.put(PRODUCT_CODE_ASPIRE_T, "Aspire");
        mapProducts.put(PRODUCT_CODE_NEW_REVOLUTION, "New Revolution");
        mapProducts.put(PRODUCT_CODE_NEW_REVOLUTION_T, "New Revolution");
        mapProducts.put(PRODUCT_CODE_NEW_REVOLUTION_PLUS, "New Revolution Plus");
        mapProducts.put(PRODUCT_CODE_REVOLUTION, "Revolution");
        mapProducts.put(PRODUCT_CODE_VOL2, "Volume-2");
        mapProducts.put(PRODUCT_CODE_VOL3, "Best of Kishore Kumar");
        mapProducts.put(PRODUCT_CODE_VOL4, "Best of Mohammed Rafi");
        mapProducts.put(PRODUCT_CODE_VOL5, "Best of Mukesh & Asha Bhonsle");
        mapProducts.put(PRODUCT_CODE_VOL6, "Best of Lata Mangeshkar");
        mapProducts.put(PRODUCT_CODE_VOL7, "Classical Song Collection");
        mapProducts.put(PRODUCT_CODE_VOL8, "Latest Songs");
        mapProducts.put(PRODUCT_CODE_VOL9, "Oldies to Goldies");
        mapProducts.put(PRODUCT_CODE_VOL10, "Legendary Singers");
        mapProducts.put(PRODUCT_CODE_VOL11, "Combo");
        mapProducts.put(PRODUCT_CODE_VOL3456, "Volume-3-4-5-6");
        mapProducts.put(PRODUCT_CODE_VOL34567, "Volume-3-4-5-6-7");
        mapProducts.put(PRODUCT_FAVORITES, "Favorites");
    }



    public final static String TABLE_NAME = "SongBookMaster";
    public final static String COLUMN_ID = "_id";
    public final static String COLUMN_SONG_NUMBER = "SongNumber";
    public final static String COLUMN_SONG_LANGUAGE = "Language";
    public final static String COLUMN_SONG_NAME = "SongNameLine1";
    public final static String COLUMN_SONG_SINGER = "Singer";
    public final static String COLUMN_SONG_COMPOSER = "Music";
    public final static String COLUMN_SONG_ALBUM = "Movie/Album";
    public final static String COLUMN_SONG_PRODUCT = "Product Name";
    public final static String COLUMN_SONG_PRODUCT_CODE = "Product Code";

}
