package Database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import Utils.Constants;

/**
 * Created by Tanay on 24/03/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.pardypanda.songbook/databases/";

    private static String DB_NAME = "Persang_v4.1.db";
    private final Context myContext;
    private SQLiteDatabase myDataBase;

    private SharedPreferences mSharedPrefs;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    public DatabaseHelper(Context context) {

        super(context, DB_NAME, null, 1);
        this.myContext = context;

        mSharedPrefs = context.getSharedPreferences(Constants.KEY_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if (dbExist) {
            //do nothing - database already exist
        } else {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);

        } catch (SQLiteException e) {

            //database does't exist yet.

        }

        if (checkDB != null) {

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     */
    private void copyDataBase() throws IOException {

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    public Cursor executeSQL(String sqlQuery, String[] selectionArgs) {
        return myDataBase.rawQuery(sqlQuery, selectionArgs);
    }

    @Override
    public synchronized void close() {

        if (myDataBase != null)
            myDataBase.close();

        super.close();

    }

    public String createQueryString(String selectedProduct) {
        String query = "";

        String selectQuery = "SELECT * FROM " + Constants.TABLE_NAME;

        String addVolumes = "";

        switch (selectedProduct) {
            case Constants.PRODUCT_CODE_SKIPPER:
            case Constants.PRODUCT_CODE_NEW_DZIRE_PLUS:
            case Constants.PRODUCT_CODE_DZIRE_PLUS:
            case Constants.PRODUCT_CODE_DZIRE_PLUS_T:
            case Constants.PRODUCT_CODE_DZIRE:
            case Constants.PRODUCT_CODE_DZIRE_T:
            case Constants.PRODUCT_CODE_NEW_DZIRE_T:
            case Constants.PRODUCT_CODE_ASPIRE_PLUS:
            case Constants.PRODUCT_CODE_ASPIRE_PLUS_T:
            case Constants.PRODUCT_CODE_ASPIRE:
            case Constants.PRODUCT_CODE_ASPIRE_T:
            case Constants.PRODUCT_CODE_NEW_REVOLUTION:
            case Constants.PRODUCT_CODE_NEW_REVOLUTION_T:
            case Constants.PRODUCT_CODE_NEW_REVOLUTION_PLUS:
            case Constants.PRODUCT_CODE_REVOLUTION:
            case Constants.PRODUCT_CODE_VOL11:
                addVolumes = " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL3 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL4 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL5 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL6 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL7 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL8 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL9 + "' ";
                break;

            case Constants.PRODUCT_CODE_VOL3456:
                addVolumes = " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL3 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL4 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL5 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL6 + "' ";
                break;

            case Constants.PRODUCT_CODE_VOL34567:
                addVolumes = " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL3 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL4 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL5 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL6 + "' "
                        + " OR `" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = '" + Constants.PRODUCT_CODE_VOL7 + "' ";
                break;
        }

        String whereClause = " WHERE (`" + Constants.COLUMN_SONG_PRODUCT_CODE + "` = ? " + addVolumes + ")";

        query = selectQuery + whereClause;

        return query;
    }

    public String createFilterQuery(String selectedProduct, CharSequence charSequence) {
        String filterQuery = "";

        if (selectedProduct.equals(Constants.PRODUCT_FAVORITES)) {
            filterQuery = getQueryFavorites();
        } else {
            filterQuery = createQueryString(selectedProduct);
        }

        String filter = " AND "
                + "("
                + "`" + Constants.COLUMN_SONG_NUMBER + "`" + " LIKE '%" + charSequence + "%'"
                + " OR "
                + "`" + Constants.COLUMN_SONG_NAME + "`" + " LIKE '%" + charSequence + "%'"
                + " OR "
                + "`" + Constants.COLUMN_SONG_SINGER + "`" + " LIKE '%" + charSequence + "%'"
                + " OR "
                + "`" + Constants.COLUMN_SONG_COMPOSER + "`" + " LIKE '%" + charSequence + "%'"
                + " OR "
                + "`" + Constants.COLUMN_SONG_ALBUM + "`" + " LIKE '%" + charSequence + "%'"
                + " OR "
                + "`" + Constants.COLUMN_SONG_LANGUAGE + "`" + " LIKE '%" + charSequence + "%'"
                + " OR "
                + "`" + Constants.COLUMN_SONG_PRODUCT + "`" + " LIKE '%" + charSequence + "%'"
                + ")";

        return filterQuery + filter;
    }

    public Cursor getSongs(String selectedProduct) {

        //String orderBy = " ORDER BY `" + Constants.COLUMN_SONG_NAME + "` ";
        String orderBy = " ORDER BY CASE WHEN `" + Constants.COLUMN_SONG_NAME + "` "
                + "COLLATE NOCASE BETWEEN 'a' AND 'zzzzz' "
                + "THEN `" + Constants.COLUMN_SONG_NAME + "` ELSE '~' || `" + Constants.COLUMN_SONG_NAME + "` "
                + "END COLLATE NOCASE ";

        return executeSQL(createQueryString(selectedProduct) + orderBy, new String[]{selectedProduct});
    }

    public String getQueryFavorites() {
        String selectQuery = "SELECT * FROM " + Constants.TABLE_NAME;

        Set<String> setFavorites = mSharedPrefs.getStringSet(Constants.KEY_SET_FAVORITES, null);

        String csvFavorites = "";
        if (setFavorites != null && setFavorites.size() > 0) {
            for (String songNumber : setFavorites) {
                csvFavorites += (csvFavorites == "" ? "" : ",") + songNumber;
            }
        }
        String whereClause = " WHERE `" + Constants.COLUMN_SONG_NUMBER + "` IN (" + csvFavorites + ")";

        return selectQuery + whereClause;
    }

    public Cursor getFavorites() {
        String groupBy = " GROUP BY " + Constants.COLUMN_SONG_NUMBER;
        return executeSQL(getQueryFavorites() + groupBy, new String[]{});
    }

    public Cursor filterQuery(String selectedProduct, CharSequence charSequence) {

        //String orderBy = " ORDER BY `" + Constants.COLUMN_SONG_NAME + "` ";
        String orderBy = " ORDER BY CASE WHEN `" + Constants.COLUMN_SONG_NAME + "` "
                + "COLLATE NOCASE BETWEEN 'a' AND 'zzzzz' "
                + "THEN `" + Constants.COLUMN_SONG_NAME + "` ELSE '~' || `" + Constants.COLUMN_SONG_NAME + "` "
                + "END COLLATE NOCASE ";

        if (selectedProduct.equals(Constants.PRODUCT_FAVORITES)) {
            String groupBy = " GROUP BY " + Constants.COLUMN_SONG_NUMBER;
            return executeSQL(createFilterQuery(selectedProduct, charSequence) + groupBy + orderBy, new String[]{});
        } else {
            return executeSQL(createFilterQuery(selectedProduct, charSequence) + orderBy, new String[]{selectedProduct});
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // Add your public helper methods to access and get content from the database.
    // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
    // to you to create adapters for your views.

}
